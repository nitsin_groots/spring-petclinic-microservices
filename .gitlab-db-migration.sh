#!/bin/bash
#Get servers list
set -fex
string=$JUMP_SERVER
array=(${string//,/ })
for i in "${!array[@]}"
do       
      scp -r ./db_changelogs/ ubuntu@${array[i]}:/home/ubuntu/test_db_migration/
      ssh ubuntu@${array[i]} "/bin/bash /home/ubuntu/test_db_migration/db_update.sh"
   
done
