-- liquibase formatted sql


-- changeset tenant_master:change-1
ALTER TABLE cities ADD COLUMN state_id INT NOT NULL;

-- changeset tenants_master:change-2
-- comment: Create new table user
CREATE TABLE user
( id INT NOT NULL,
  name VARCHAR(30) NOT NULL
);

-- changeset tenants_master:change-3
-- comment: Create new table acl_class
CREATE TABLE acl_class
( id INT NOT NULL,
  class VARCHAR(100) NOT NULL
);
