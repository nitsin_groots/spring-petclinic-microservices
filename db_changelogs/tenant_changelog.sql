-- liquibase formatted sql

-- changeset tenants:change-1
ALTER TABLE Tenant_details ADD COLUMN phone INT NOT NULL;


-- changeset tenants:change-2
ALTER TABLE Tenant_details ADD COLUMN email VARCHAR(20) NOT NULL;

-- changeset tenants:change-3
ALTER TABLE Tenant_details ADD COLUMN address VARCHAR(20) NOT NULL;

-- changeset tenants:change-4
ALTER TABLE Tenant_details ADD COLUMN country VARCHAR(20) NOT NULL;


-- changeset tenants:change-5
-- comment: Create new table user
CREATE TABLE user
( id INT NOT NULL,
  name VARCHAR(30) NOT NULL
);

-- changeset tenants:change-6
ALTER TABLE Tenant_details ADD COLUMN city VARCHAR(20) NOT NULL;
ALTER TABLE user ADD COLUMN email VARCHAR(20) NOT NULL;

-- changeset tenants:change-7
-- comment: drop column country from Tenant_details
 ALTER TABLE Tenant_details DROP COLUMN country;


-- changeset tenants:change-8
-- comment: Create new table account 
CREATE TABLE account
( id INT NOT NULL,
  title VARCHAR(30) NOT NULL
);

-- changeset tenants:change-9
-- comment: Add new column phone to user table
ALTER TABLE user ADD COLUMN phone INT NOT NULL;

