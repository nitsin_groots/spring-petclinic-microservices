#!/bin/bash
#Get servers list
set -fe
string=$JUMP_SERVER
array=(${string//,/ })
for i in "${!array[@]}"
do    
      echo "Deploy project on server ${array[i]}"    
      #ssh ubuntu@${array[i]} "sudo mkdir /uat" 
      #ssh ubuntu@${array[i]} "sudo chown -R ubuntu:ubuntu /uat"
     
      scp -r ./spring-petclinic-admin-server/target/ ubuntu@${array[i]}:/home/ubuntu/test_artifacts/admin/
      scp -r ./spring-petclinic-customers-service/target/ ubuntu@${array[i]}:/home/ubuntu/test_artifacts/customer/
      scp -r ./spring-petclinic-visits-service/target/ ubuntu@${array[i]}:/home/ubuntu/test_artifacts/visits/
      scp -r ./spring-petclinic-visits-service/target/ ubuntu@${array[i]}:/home/ubuntu/test_artifacts/vets/

      #./spring-petclinic-admin-server/target/*.jar
      #./spring-petclinic-customers-service/target/*.jar
      #./spring-petclinic-visits-service/target/*.jar
      #./spring-petclinic-vets-service/target/*.jar
      ssh ubuntu@${array[i]} "/bin/bash /home/ubuntu/test_artifacts/test_deploy.sh"
done
